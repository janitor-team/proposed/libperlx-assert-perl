libperlx-assert-perl (0.905-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.
  * debian/*: update GitHub URLs to use HTTPS.

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.
  * Set Testsuite header for perl package.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 00:04:34 +0100

libperlx-assert-perl (0.905-1) unstable; urgency=medium

  * Team upload.

  * Add libkeyword-simple-perl, libdevel-declare-perl to Depends.
    (Closes: #868075)
  * Bump debhelper compatibility level to 9.
  * Use canonical URL for Vcs-Git.
  * Declare compliance with Debian Policy 4.0.0.

  * New upstream version 0.905.
    Fixes "FTBFS with perl 5.26: Unescaped left brace in regex"
    (Closes: #826489)
  * Update years of upstream copyright.
  * Drop mention of removed CONTRIBUTING file from debian/copyright.
  * debian/gbp.conf: replace git-import-orig with import-orig.

 -- gregor herrmann <gregoa@debian.org>  Tue, 11 Jul 2017 23:09:48 +0200

libperlx-assert-perl (0.904-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#779132.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 24 Feb 2015 19:28:25 +0100
